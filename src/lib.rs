/*
El sistema de quadrícules SOC es defineix per una xarxa de línies paral·leles als eixos
de coordenades UTM31 amb origen al punt de coordenades (250000, 4750000) i
orientació de les quadrícules oest-est i nord-sud.
Es tracta d’un sistema jeràrquic amb resolucions d’1 i 5 km. El sistema de quadrícules
es denomina SOC i la identificació del nivell de quadrícula és concreta a través del
propi identificador.

 * SOC5: Quadrícula de 5x5 km

   D’oest a est des de l’origen és defineixen franges verticals de 5 km que
   s’identifiquen per dos dígits començant per 01 fins 56.

   De nord a sud des de l’origen és defineixen franges horitzontals de 5 km que
   s’identifiquen per dues de lletres essent la primera AA i la darrera BZ.

   Cada cel·la de la quadrícula de 5 km de costat o quadrant és el resultat de la
   intersecció d’una franja vertical i una altra horitzontal, s’identifica amb la
   concatenació dels codis: dos dígits i dues lletres.

 * SOC1: Quadrícula d’1x1 km

   Cadascun dels quadrants de 25 km2, es subdivideix en 25 cel·les quadrades
   d’1 km2.

   Cada cel·la de la quadrícula d’1 km de costat o secció es codifica d’oest a est i
   de nord a sud amb dos dígits de 01 a 25.
*/

struct FixedLengthString<const N: usize>([u8; N]);
/*
impl<const N: usize> FixedLengthString<N> {
    pub const fn as_bytes(&self) -> &[u8] {
        &self.0
    }
}
*/
impl<const N: usize> core::ops::Deref for FixedLengthString<N> {
    type Target = str;
    fn deref(&self) -> &str {
        core::str::from_utf8(&self.0).expect("Invariant was violated")
    }
}
impl<const N: usize> TryFrom<[u8; N]> for FixedLengthString<N> {
    type Error = core::str::Utf8Error;
    fn try_from(input: [u8; N]) -> Result<Self, <Self as TryFrom<[u8; N]>>::Error> {
        core::str::from_utf8(&input)?;
        Ok(Self(input))
    }
}
impl<const N: usize> core::fmt::Display for FixedLengthString<N> {
    fn fmt(&self, formatter: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
        let as_str: &str = self;//implicit deref
        core::fmt::Display::fmt(as_str, formatter)
    }
}

fn soc_soc5y_to_ascii(y: u32) -> FixedLengthString<2> {
    let first: u8 = (y / 26).try_into().unwrap();
    let second: u8 = (y % 26).try_into().unwrap();
    [b'A' + first, b'A' + second]
        .try_into()
        .expect("Unexpected error")
}

fn soc_ascii_to_soc5y(input: FixedLengthString<2>) -> u32 {
    let input = input.as_bytes();
    assert!(input[0] >= b'A' && input[0] <= b'Z');
    assert!(input[1] >= b'A' && input[1] <= b'Z');
    let first: u32 = (input[0] - b'A').into();
    let second: u32 = (input[1] - b'A').into();
    first * 26 + second
}

//easting, northing
#[derive(Copy, Clone, Debug)]
pub struct UTM31NCoordinate(u32, u32);
#[derive(Copy, Clone, Debug)]
pub struct UTM31NSquare {
    north: u32,
    south: u32, //non inclusive
    east: u32,
    west: u32, //non inclusive
}
impl UTM31NSquare {
    pub fn contains(&self, coordinate: UTM31NCoordinate) -> bool {
        (coordinate.0 >= self.east && coordinate.0 < self.west)
            && (coordinate.1 >= self.south && coordinate.1 < self.north)
    }
}

// l'origen del SOC és al Nord-est
const X_ORIG: u32 = 250000;
const Y_ORIG: u32 = 4750000;

pub fn soc_to_utm(input: &str) -> UTM31NSquare {
    let a = input.get(0..2).unwrap();
    let b: [u8; 2] = input.get(2..4).unwrap().as_bytes().try_into().unwrap();
    let b: FixedLengthString<2> = FixedLengthString::<2>::try_from(b).unwrap();
    let c = input.get(4..6).unwrap();
    //TODO: validate ranges
    let soc5_x = a.parse::<u32>().unwrap() - 1;
    let soc5_y = soc_ascii_to_soc5y(b);
    let sq = c.parse::<u32>().unwrap() - 1;
    let soc1_in_soc5_x = sq % 5;
    let soc1_in_soc5_y = sq / 5;
    let soc1_x = soc5_x * 5 + soc1_in_soc5_x;
    let soc1_y = soc5_y * 5 + soc1_in_soc5_y;
    //calculate NE corner
    let utm_x = X_ORIG + soc1_x * 1000;
    let utm_y = Y_ORIG - soc1_y * 1000;
    UTM31NSquare {
        north: utm_y,
        south: utm_y - 1000,
        east: utm_x,
        west: utm_x + 1000,
    }
}

#[derive(thiserror::Error,Debug)]
pub enum UtmToSocError{
    #[error("Input is west from SOC coordinates limits.")]
    TooWest,
    #[error("Input is east from SOC coordinates limits.")]
    TooEast,
    #[error("Input is north from SOC coordinates limits.")]
    TooNorth,
    #[error("Input is south from SOC coordinates limits.")]
    TooSouth,
}

pub fn utm_to_soc(x: u32, y: u32) -> Result<String, UtmToSocError> {
    if x<X_ORIG{
        return Err(UtmToSocError::TooWest)
    }
    let tx = (x - X_ORIG) / 1000;
    if Y_ORIG < y {
        return Err(UtmToSocError::TooNorth)
    }
    let ty = (Y_ORIG - y) / 1000;

    let soc5_x = tx / 5 + 1;
    if soc5_x > 56 {
         return Err(UtmToSocError::TooEast)
    }
    let soc5_y = ty / 5;
    if soc5_y > 77 /*26*2+25*/{
        return Err(UtmToSocError::TooSouth)
    }

    let soc1_x = tx % 5;
    let soc1_y = ty % 5;
    let soc1 = soc1_y * 5 + soc1_x + 1;
    Ok(
    format!(
        "{:0>2}{:0>2}{:0>2}",
        soc5_x,
        &soc_soc5y_to_ascii(soc5_y),
        soc1
    ))
}

#[cfg(test)]
mod test {
    use super::*;
    fn utm_to_soc_check(x: u32, y: u32, soc: &str) {
        dbg!(x);
        dbg!(y);
        let soc_result = utm_to_soc(x, y).unwrap();
        assert_eq!(soc_result, soc);
        let square = soc_to_utm(&soc_result);
        dbg!(square);
        assert!(square.contains(UTM31NCoordinate(x, y)));
    }
    #[test]
    fn test_utm_to_soc() {
        utm_to_soc_check(416145, 4605107, "34BC22");
        utm_to_soc_check(420440, 4592662, "35BF11");
        utm_to_soc_check(412492, 4606338, "33BC18");
        utm_to_soc_check(416624, 4649607, "34AU02");
        utm_to_soc_check(402867, 4663945, "31AR08");
    }
}
